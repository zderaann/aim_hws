from PIL import Image
import numpy as np
import os
import argparse
import math
import matplotlib.pyplot as plt

# Clamps value between low and high
def clamp(low, high, val):
    return max(low, min(high, val))

# Returns image histogram as a list of occurences
def histogram(img):
    data = list(img.getdata())
    hist = [data.count(i) for i in range(0,256)]
    return hist

# CDF 
def cdf(val, hist, w, h):
    valsum = 0.0
    for i in range(0, val + 1):
        valsum += hist[i] / (w * h)
    return valsum

# Calculate CDF for all values
def all_cdfs(hist, w, h):
    res = []
    for k in range(0,256):
            valsum = 0.0
            for i in range(0, k + 1):
                valsum += hist[i] / (w * h)
            res.append(valsum)
    return res

# Create negative image
def negative(img, out_img):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            out_img.putpixel((x,y), 255 - img.getpixel((x,y)))
    return out_img

# Threshold l image
def threshold(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            out_img.putpixel((x,y), 255 if img.getpixel((x,y)) >= l else 0)
    return out_img

# Change image brightness by l
def brightness(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            out_img.putpixel((x,y), clamp(0, 255, int((img.getpixel((x,y))/255 + l) * 255)))
    return out_img

# Change image contrast by l
def contrast(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            out_img.putpixel((x,y), clamp(0, 255, int(img.getpixel((x,y)) * l)))
    return out_img

# Gamma correction by l
def gamma_correction(img, out_img, l): 
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            out_img.putpixel((x,y), clamp(0, 255, int(((img.getpixel((x,y))/255) ** l) * 255)))
    return out_img

# Non linear contrast of an image (l = alpha)
def non_linear_contrast(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            gamma = 1/(1-l)
            pixel = img.getpixel((x,y))/255
            if pixel < 0.5:
                val = int(0.5 * ((2 * pixel) ** gamma) * 255)
                out_img.putpixel((x,y), clamp(0, 255, val))
            else:
                val = int((1 - 0.5 * ((2 - 2 * pixel) ** gamma)) * 255)
                out_img.putpixel((x,y), clamp(0, 255, val))
    return out_img

# Logarithmic scale of image
def logarithmic_scale(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            pixel = img.getpixel((x,y))/255
            out_img.putpixel((x,y), clamp(0, 255, int((math.log(1 + pixel * l) / math.log(1 + l) * 255))))
    return out_img

# Quantization of image palette to l shades
def quantization(img, out_img, l):
    w = img.width
    h = img.height
    for x in range(0,w):
        for y in range(0,h):
            val = int(math.floor(img.getpixel((x,y))/255 * l))
            out_img.putpixel((x,y), clamp(0, 255, int(val * (255/l)) ))
    return out_img

# Image equalization
def equalization(img, out_img): 
    w = img.width
    h = img.height
    hist = histogram(img)
    cdfs = all_cdfs(hist, w, h)
    for x in range(0,w):
        for y in range(0,h):
            valsum = cdfs[img.getpixel((x,y))]
            out_img.putpixel((x,y), clamp(0, 255, int(valsum * 255)))
    return out_img

# Map image histogram to histogram of image B
def mapping(img, out_img, img_B): 
    w = img.width
    h = img.height

    hist = histogram(img)
    hist_B = histogram(img_B)
    cdfA = all_cdfs(hist, w, h)
    cdfB = all_cdfs(hist_B, w, h)
    for x in range(0,w):
        for y in range(0,h):
            cdfA_val = cdfA[img.getpixel((x,y))]
            closest = min(cdfB, key=lambda x:abs(x-cdfA_val))
            index = cdfB.index(closest)
            out_img.putpixel((x,y), clamp(0, 255, index))
    return out_img

# Define arguments
parser = argparse.ArgumentParser(description='Monadic operations')
parser.add_argument('--image_path',
                    required = True,
                    help = 'Path to the image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output image file')
parser.add_argument('--operation',
                    required = True,
                    help = 'Type of operation: negative, threshold, brightness, contrast, gamma_correction, non_linear_contrast, logarithmic_scale, quantization, equalization, mapping')
parser.add_argument('--parameter',
                    required = False,
                    help = 'Parameter for the operation')

# Parse arguments
args = parser.parse_args()
imgpath = args.image_path

if not os.path.isfile(imgpath):
            print('File not found')
            exit()

# Process image
with Image.open(imgpath) as img:
    op = args.operation
    out_img = Image.new('L', img.size)
    if op == 'negative':
        out_img = negative(img, out_img)

    elif op == 'threshold':
        if args.parameter is None:
            print('Threshold parameter required')
            exit()
        threshold(img, out_img, int(args.parameter))

    elif op == 'brightness':
        if args.parameter is None:
            print('Brightness parameter required')
            exit()
        l = float(args.parameter)

        if l < -1.0 or l > 1:
            print('Brightness parameter incorrect (needs to be in interval <-1,1>)')
            exit()

        brightness(img, out_img, l)

    elif op == 'contrast':
        if args.parameter is None:
            print('Contrast parameter required')
            exit()
        l = float(args.parameter)

        if l < 0:
            print('Contrast parameter incorrect (needs to be in interval <0,inf))')
            exit()

        contrast(img, out_img, l)

    elif op == 'gamma_correction':
        if args.parameter is None:
            print('Gamma correction parameter required')
            exit()
        l = float(args.parameter)

        if l <= 0:
            print('Gamma correction parameter incorrect (needs to be in interval (0,inf))')
            exit()
        gamma_correction(img, out_img, l)

    elif op == 'non_linear_contrast':
        if args.parameter is None:
            print('Non-linear contrast parameter required')
            exit()

        l = float(args.parameter)

        if l <= 0 or l >= 1:
            print('Non-linear contrast parameter incorrect (needs to be in interval (0,1))')
            exit()

        non_linear_contrast(img, out_img, l)

    elif op == 'logarithmic_scale':
        if args.parameter is None:
            print('Logarithmic scale parameter required')
            exit()
        l = float(args.parameter)

        if l <= -1:
            print('Logarithmic scale parameter incorrect (needs to be in interval (-1,inf))')
            exit()
        logarithmic_scale(img, out_img, l)

    elif op == 'quantization':
        if args.parameter is None:
            print('Quantization parameter required')
            exit()
        l = int(args.parameter)

        if 1 <= 0:
            print('Quantization parameter incorrect (needs to be in interval (1,inf))')
            exit()
        quantization(img, out_img, l)

    elif op == 'equalization':
        equalization(img, out_img)
    elif op == 'mapping':
        if args.parameter is None:
            print('Mapping parameter required')
            exit()
        with Image.open(args.parameter) as second_img:
            mapping(img, out_img, second_img)
            
            hist_B = histogram(second_img)
            fig2 = plt.figure(2)             
            for i in range(0, 256):
                plt.bar(i, hist_B[i])
            fig2.suptitle('Histogram of second image', fontsize=16)
            
    else:
        print('Unknown type of operation')
        exit()

    # Save image
    out_img.save(args.output_path)
    
    # Plot histograms of images
    img_hist = histogram(img)
    fig0 = plt.figure(0)             
    for i in range(0, 256):
        plt.bar(i, img_hist[i])
    out_img_hist = histogram(out_img)
    fig0.suptitle('Histogram of input image', fontsize=16)

    fig1 = plt.figure(1)             
    for i in range(0, 256):
        plt.bar(i, out_img_hist[i])
    fig1.suptitle('Histogram of output image', fontsize=16)
    plt.show()
