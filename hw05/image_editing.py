from PIL import Image
import argparse
import numpy as np

# Clamp value between low and high
def clamp(low, high, val):
    return max(low, min(high, val))

# Calculate gradient
def calculate_gradient(img, axis):
    data = np.array(img, np.int8)
    s = data.shape
    data_tmp = np.zeros(s, np.int8)
    if not axis:
        for y in range(s[0]):
            for x in range(s[1]):
                data_tmp[y,x] = data[y,x] - data[y,clamp(0,s[1] - 1, x+1)]
    else:
                
        for x in range(s[1]):
            for y in range(s[0]):
                data_tmp[y,x] = data[y,x] - data[clamp(0,s[0] - 1, y+1), x]


    return data_tmp

# Stitch gradients
def merge_gradients(grad1, grad2, xmin, xmax, ymin, ymax):
    grad1[ymin:ymax, xmin:xmax] = grad2[ymin:ymax, xmin:xmax]
    return grad1

# Calculate intensity
def gauss_seidel(img, w,h, gradX, gradY, xmin, xmax, ymin, ymax):
    diff = np.array([1,1,1]) * 900000
    I = np.array(img, np.float)

    ggX = calculate_gradient(gradX, 0)
    ggY = calculate_gradient(gradY, 1)

    print(ggX.shape)
    print(ggY.shape)
    print((np.amin(ggX), np.amin(ggY)))
    div = ggX + ggY

    print(div.shape)
    print((np.amin(div), np.amax(div)))
    first_pass = True
    
    while sum(diff) > 0.0001:
        newdiff = 0
        for y in range(ymin-2, ymax+2):
            for x in range(xmin-2, xmax+2):
                val = (I[y, clamp(0, w-1, x+1)] + I[y, clamp(0,w-1, x-1)] + I[clamp(0,h-1,y+1), x] + I[clamp(0, h-1, y-1), x] - div[y,x]) / 4
                newdiff += abs(I[y,x] - val)
                I[y,x] = val
        if first_pass:
            first_pass = False
            diff = newdiff
        else:
            diff = abs(newdiff - diff)
        print(sum(diff))

    print((np.amin(I),np.amax(I)))

    return I.astype(np.uint8)
        
# Stitch images
def merge_images(img1, img2, xmin, xmax, ymin, ymax):
    data1 = np.array(img1)
    data2 = np.array(img2)
    data1[ymin:ymax, xmin:xmax] = data2[ymin:ymax, xmin:xmax]
    return data1


# Define arguments
parser = argparse.ArgumentParser(description='Gaussian convolution')
parser.add_argument('--image_path1',
                    required = True,
                    help = 'Path to the first image file')
parser.add_argument('--image_path2',
                    required = True,
                    help = 'Path to the second image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output file')
parser.add_argument('--xmin', type=int, default=192,
                    help='Minimal X coordinate of stitching window')
parser.add_argument('--ymin', type=int, default=107,
                    help='Minimal Y coordinate of stitching window')
parser.add_argument('--xmax', type=int, default=251,
                    help='Maximal X coordinate of stitching window')
parser.add_argument('--ymax', type=int, default=148,
                    help='Maximal Y coordinate of stitching window')

# Parse arguments
args = parser.parse_args()
imgpath1 = args.image_path1
imgpath2 = args.image_path2

xmin = args.xmin
ymin = args.ymin
xmax = args.xmax
ymax = args.ymax

# Process image
img1 =  Image.open(imgpath1)
img2 =  Image.open(imgpath2)

gradient1x = calculate_gradient(img1, 0)
gradient1y = calculate_gradient(img1, 1)

gradient2x = calculate_gradient(img2, 0)
gradient2y = calculate_gradient(img2, 1)

'''gradient1x_im = Image.fromarray(gradient1x)
gradient1x_im.save('./gradient1x2.jpg')

gradient1y_im = Image.fromarray(gradient1y)
gradient1y_im.save('./gradient1y2.jpg')

gradient2x_im = Image.fromarray(gradient2x)
gradient2x_im.save('./gradient2x2.jpg')

gradient2y_im = Image.fromarray(gradient2y)
gradient2y_im.save('./gradient2y2.jpg') '''

gradmix_x = merge_gradients(gradient1x, gradient2x, xmin, xmax, ymin, ymax)
gradmix_y = merge_gradients(gradient1y, gradient2y, xmin, xmax, ymin, ymax)
img_mix = merge_images(img1, img2, xmin, xmax, ymin, ymax)

'''
print(gradmix_x.shape)
gradmix_x_im = Image.fromarray(gradmix_x)
gradmix_x_im.save('./gradientmix_x.jpg')

gradmix_y_im = Image.fromarray(gradmix_y)
gradmix_y_im.save('./gradientmix_y.jpg')'''

intensity = gauss_seidel(img_mix, img1.width, img1.height, gradmix_x, gradmix_y, xmin, xmax, ymin, ymax)
out_img = Image.fromarray(intensity, 'RGB')
out_img.save(args.output_path)

img1.close()
img2.close()
