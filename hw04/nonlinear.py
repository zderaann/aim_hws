from PIL import Image
import argparse
import numpy as np
import math

# Returns image histogram as a list of occurences
def histogram(data):
    hist = [(data == i).sum() for i in range(0,256)]
    return np.array(hist)

# CLamps value between low and high
def clamp(low, high, val):
    return max(low, min(high, val))

# Calculate 2D kernel matrix of size 2*n+1 x 2*n+1
def calculate_2Dkernel(n, sigma):
    # Compute kernel elements
    row = [math.exp( -(((x)/(sigma))**2)/2.0 ) for x in range(int(-(2*n+1)/2), int((2*n+1)/2) + 1, 1)] # v centru vychazi 1, vyzkouseno na kernelech o radiu 1-5

    kernel_arr = [[xh*xv for xh in row] for xv in row]
    kernel = np.array(kernel_arr)

    # Normalize the kernel
    kernelsum = np.sum(kernel)
    return kernel / kernelsum

# 1D gaussian
def b(val, sigma):
    return math.exp( -(((val)/(sigma))**2)/2.0 )

# Bruteforce bilateral filter
def bilateral_bruteforce(img, kernel, sigmab):
    w = img.width
    h = img.height
    out_img = Image.new('L', img.size)

    for x in range(0,w):
        for y in range(0,h):
            res = 0.0
            s = kernel.shape
            W = 0
            for i in range(0, s[0]):
                for j in range(0, s[1]):
                  index_x = x - int(s[0]/2) + i
                  index_x = index_x if index_x > 0 else 0
                  index_y = y - int(s[1]/2) + j
                  index_y = index_y if index_y > 0 else 0
                  
                  pixel = img.getpixel((index_x if index_x < w else w - 1 ,  index_y if index_y < h else h - 1))
                  val = kernel[i][j] * b(pixel - img.getpixel((x,y)), sigmab)
                  res += val * pixel
                  W += val
            out_img.putpixel((x,y), int(res / W))

    return out_img

# Box kernel convolution
def bilateral_boxkernel(img, n, sigmab):
    w = img.width
    h = img.height
    out_img = Image.new('L', img.size)

    tmp = np.array(img)
    hist = np.array([])

    for x in range(0,w):
        
        index_xmin = x - n
        index_xmax = x + n
        indexes_x = [clamp(0,w-1,i) for i in range(index_xmin,index_xmax + 1)]

        # Moving up and down
        y_start = 0
        y_end = h
        y_step = 1

        if x % 2 == 1:
            y_start = h - 1
            y_end = -1
            y_step = -1

        for y in range(y_start, y_end, y_step):                        
            index_ymin = y - n
            index_ymax = y + n
            indexes_y = [clamp(0,h-1,i) for i in range(index_ymin,index_ymax + 1)]

            # Calculate histogram of image window 
            if hist.size == 0:
                a = np.take(tmp, indexes_x, 1)
                aa = np.take(a, indexes_y, 0)            
                hist = histogram(aa) 
            
            pixel = img.getpixel((x,y))
            res = sum([hist[i] * b(i - pixel, sigmab) for i in range(256)])

            # Shift image histogram window down
            if x % 2 == 0:
                index_ymax = index_ymax + 1 if index_ymax + 1 < h else h - 1
                hist = hist - histogram(tmp[indexes_y[0],indexes_x])
                hist = hist + histogram(tmp[index_ymax,indexes_x])
                indexes_y = indexes_y[1:2*n+2]
                indexes_y.append(index_ymax)

            # Shift image histogram window up
            else:
                index_ymax = index_ymin - 1 if index_ymin - 1 > 0 else 0
                hist = hist - histogram(tmp[indexes_y[-1],indexes_x])
                hist = hist + histogram(tmp[index_ymax,indexes_x])
                indexes_y = indexes_y[0:2*n+1]
                indexes_y.insert(0, index_ymax)

            out_img.putpixel((x,y), int(res))

        # Shift image histogram window to the right
        index_xmax = index_xmax + 1 if index_xmax + 1 < w else w - 1

        hist = hist - histogram(tmp[indexes_y,indexes_x[0]])
        hist = hist + histogram(tmp[indexes_y,index_xmax])
        
    return out_img

# Define arguments
parser = argparse.ArgumentParser(description='Gaussian convolution')
parser.add_argument('--image_path',
                    required = True,
                    help = 'Path to the image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output file')
parser.add_argument('--kernel_radius',
                    required = True,
                    help = 'Radius of the Gaussian kernel')
parser.add_argument('--sigma',
                    required = True,
                    help = 'Sigma for Gaussian kernel')
parser.add_argument('--sigmab',
                    required = True,
                    help = 'Sigma for Gaussian of b')
parser.add_argument('--method',
                    required = True,
                    help = 'Method for convolution (brutforce, separable_kernel)')

# Parse arguments
args = parser.parse_args()
imgpath = args.image_path

# Process image
with Image.open(imgpath) as img:
    # Bruteforce
    if args.method == 'bruteforce':
        kernel = calculate_2Dkernel(int(args.kernel_radius), float(args.sigma)) 
        out_img = bilateral_bruteforce(img, kernel, float(args.sigmab))

    # Box kernel aproximation
    elif args.method == 'box_kernel':
        out_img = bilateral_boxkernel(img, int(args.kernel_radius), float(args.sigmab))
    # Unkown method
    else:
      print('Unkown type of method, please select bruteforce or box_kernel')
      exit()
    
    # Save image
    out_img.save(args.output_path)
