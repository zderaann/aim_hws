from PIL import Image
import numpy as np
import argparse
import math

# Calculate 2D kernel matrix of size 2*n+1 x 2*n+1
def calculate_2Dkernel(n, sigma):
    # Compute kernel elements
    row = [math.exp( -(((x)/(sigma))**2)/2.0 ) for x in range(int(-(2*n+1)/2), int((2*n+1)/2) + 1, 1)] # v centru vychazi 1, vyzkouseno na kernelech o radiu 1-5

    kernel_arr = [[xh*xv for xh in row] for xv in row]
    kernel = np.array(kernel_arr)

    # Normalize the kernel
    kernelsum = np.sum(kernel)
    print(kernel)
    print(kernel / kernelsum)
    return kernel / kernelsum

# Calculate 1D kernel of length 2*n+1
def calculate_1Dkernel(n, sigma):
    kernel = [math.exp( -(((x)/(sigma))**2)/2.0 ) for x in range(int(-(2*n+1)/2), int((2*n+1)/2), 1)]
    return np.array(kernel) / sum(kernel)

# Separable kernel convolution
def convolution_separable_kernel(img, kernel):
    w = img.width
    h = img.height
    tmp = np.zeros((img.width, img.height))  
    out_img = Image.new('L', img.size)

    # First pass convolution
    for y in range(0,h):
      for x in range(0,w):
        val = 0.0
        t = 0.0
        for i in range(0, kernel.size):
          index = x - int(kernel.size/2) + i
          val += img.getpixel((index if index < w else w - 1 ,y)) * kernel[i] 
          t += kernel[i]
        tmp[x][y] = val / t

    # Second pass convolution
    for x in range(0,w):
      for y in range(0,h):
        val = 0.0
        t = 0.0
        for i in range(0, kernel.size):
          index = y - int(kernel.size/2) + i
          val += tmp[x][index if index < h else h - 1] * kernel[i]
          t += kernel[i]
        out_img.putpixel((x,y), int(val / t))

    return out_img


# Bruteforce convolution
def convolution_bruteforce(img, kernel):
    w = img.width
    h = img.height
    out_img = Image.new('L', img.size)

    for x in range(0,w):
        for y in range(0,h):
            res = 0.0
            s = kernel.shape
            for i in range(0, s[0]):
                for j in range(0, s[1]):
                  index_x = x - int(s[0]/2) + i
                  index_y = y - int(s[1]/2) + j
                  res += kernel[i][j] * img.getpixel((index_x if index_x < w else w - 1 ,  index_y if index_y < h else h - 1))
            out_img.putpixel((x,y), int(res))

    return out_img

# Define arguments
parser = argparse.ArgumentParser(description='Gaussian convolution')
parser.add_argument('--image_path',
                    required = True,
                    help = 'Path to the image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output file')
parser.add_argument('--kernel_radius',
                    required = True,
                    help = 'Radius of the Gaussian kernel')
parser.add_argument('--sigma',
                    required = True,
                    help = 'Sigma for Gaussian kernel')
parser.add_argument('--method',
                    required = True,
                    help = 'Method for convolution (brutforce, separable_kernel)')

# Parse arguments
args = parser.parse_args()
imgpath = args.image_path

# Process image
with Image.open(imgpath) as img:
    # Bruteforce convolution
    if args.method == 'bruteforce':
        kernel = calculate_2Dkernel(int(args.kernel_radius), float(args.sigma)) 
        out_img = convolution_bruteforce(img, kernel)

    # Separable kernel convolution
    elif args.method == 'separable_kernel':
        kernel = calculate_1Dkernel(int(args.kernel_radius), float(args.sigma))  
        out_img = convolution_separable_kernel(img, kernel)
    # Unkown method
    else:
      print('Unkown type of method, please select bruteforce or separable_kernel')
      exit()

    # Save image
    out_img.save(args.output_path)
