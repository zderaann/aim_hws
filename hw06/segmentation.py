from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import maximum_flow
from PIL import Image
import numpy as np
import argparse
#import igraph as g


# https://www.w3.org/TR/AERT/#color-contrast
def convert_rgb_to_i(pixel):
    return (0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2])/255

def calculate_weight(ip, iq):
    return 1 + int(round(min(ip, iq)))
    #return 1/(1 + (ip-iq)**2 / 64)

#scipy.sparse.csgraph.maximum_flow
def create_graph(img, s, t):
    graph = csr_matrix((img.width * img.height, img.width * img.height), dtype=np.int8)
    for y in range(img.height):
        for x in range(img.width):
            #print((x,y))
            ip = convert_rgb_to_i(img.getpixel((x, y)))
            if x - 1 >= 0:
                iq = convert_rgb_to_i(img.getpixel((x - 1, y)))
                w = calculate_weight(ip, iq)
                graph[img.width * y + x, img.width * y + x - 1] = w
                graph[img.width * y + x - 1, img.width * y + x] = w
            if x + 1 < img.width:
                iq = convert_rgb_to_i(img.getpixel((x + 1, y)))
                w = calculate_weight(ip, iq)
                graph[img.width * y + x, img.width * y + x + 1] = w
                graph[img.width * y + x + 1, img.width * y + x] = w
            if y - 1 >= 0:
                iq = convert_rgb_to_i(img.getpixel((x, y - 1)))
                w = calculate_weight(ip, iq)
                graph[img.width * y + x, img.width * (y - 1) + x] = w
                graph[img.width * (y - 1) + x, img.width * y + x] = w
            if y + 1 < img.height:
                iq = convert_rgb_to_i(img.getpixel((x, y + 1)))
                w = calculate_weight(ip, iq)
                graph[img.width * y + x, img.width * (y + 1) + x] = w
                graph[img.width * (y + 1) + x, img.width * y + x] = w
    for i in range(img.width * img.height):
        x = i % img.width
        y = int(i/img.width)
        ip = convert_rgb_to_i(img.getpixel((x, y)))
        wps = calculate_weight(ip, 0)
        wpt = calculate_weight(ip, 1)
        if i != t:
            graph[i, s] = wps
            graph[s, i] = wps
        if i != s:
            graph[i, t] = wpt
            graph[t, i] = wpt

    
    np.savetxt('./graph.txt', graph.toarray().astype(int), fmt='%d')
    #return g.Graph.Weighted_Adjacency(graph, mode='undirected', attr='weight')
    return graph

def calculate_max_flow(graph, s, t):
    return maximum_flow(graph, s, t)
    #return graph.maxflow(s, t, capacity = graph.es['weight'])

def color_image(img, graph, max_flow, s, t, s_rgb, t_rgb):
    print(max_flow.residual.toarray())
    print()
    print(graph.toarray())

    #print()
    #print(graph.toarray() - abs(max_flow.residual.toarray()))

    visited = np.zeros((img.height, img.width), dtype=np.bool)

    queue = [s]

    while len(queue) != 0:
        index = queue[0]
        del queue[0]
        print(index)
        x = index % img.width
        y = int(index/img.width)
        if not visited[y, x]:
            visited[y, x] = True
            print('---------------')
            print((x,y))
            if x - 1 >= 0:
                #e = graph.get_eid(index - 1, index)
                #f = graph.es[e]["weight"] - max_flow.flow[e]
                f = graph[index - 1, index]  - abs(max_flow.residual[index - 1, index])
                #f2 = graph[index, index - 1] - max_flow.residual[index, index - 1]
                x2 = (index- 1) % img.width
                y2 = int((index - 1)/img.width)
                #print((x2, y2))
                if f > 0 and not visited[y, x - 1]:
                    #print(f)
                    print((x2, y2, f))
                    queue.append(index - 1)

            if x + 1 < img.width:
                #e = graph.get_eid(index, index + 1)
                #f = graph.es[e]["weight"] - abs(max_flow.flow[e])
                f = graph[index + 1, index] - abs(max_flow.residual[index + 1, index])
                x2 = (index + 1) % img.width
                y2 = int((index + 1)/img.width)
                #print((x2, y2))
                if f > 0 and not visited[y, x + 1]:
                    print((x2, y2, f))
                    #print(f)
                    queue.append(index + 1)

            if y - 1 >= 0:
                #e = graph.get_eid(index - img.width, index)
                #f = graph.es[e]["weight"] - max_flow.flow[e]
                f = graph[index - img.width, index] - abs(max_flow.residual[index - img.width, index])
                x2 = (index - img.width) % img.width
                y2 = int((index - img.width)/img.width)
                #print((x2, y2))
                if f > 0 and not visited[y - 1, x]:
                    #print(f)
                    print((x2, y2, f))
                    queue.append(index - img.width)

            if y + 1 < img.height:
                #print(index, index + img.width)
                #e = graph.get_eid(index, index + img.width)
                #f = graph.es[e]["weight"] - max_flow.flow[e]
                f = graph[index + img.width, index] - abs(max_flow.residual[index + img.width, index])
                x2 = (index + img.width) % img.width
                y2 = int((index + img.width)/img.width)
                #print((x2, y2))
                if f > 0 and not visited[y + 1, x]:
                    print((x2, y2, f))
                    #print(f)
                    queue.append(index + img.width)



    print(visited)
    out_img = Image.new('RGB', img.size)
    for y in range(img.height):
        for x in range(img.width):
            i = convert_rgb_to_i(img.getpixel((x, y)))
            if visited[y,x]:
                col = (int(round(i * s_rgb[0])), int(round(i * s_rgb[1])), int(round(i * s_rgb[2])))
                out_img.putpixel((x,y), col)
            else:
                col = (int(round(i * t_rgb[0])), int(round(i * t_rgb[1])), int(round(i * t_rgb[2])))
                out_img.putpixel((x,y), col)

    return out_img

# Define arguments
parser = argparse.ArgumentParser(description='Fourier transform of the image, amplitude')
parser.add_argument('--image_path',
                    required = True,
                    help = 'Path to the image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output file')

#s_xy = (40,40)
#t_xy = (140,140)
s_xy = (1,1)
t_xy = (3,3)

#s_xy = (3,3)
#t_xy = (15,15)
s_rgb = (130, 165, 186)
t_rgb = (199, 162, 103)


# Parse arguments
args = parser.parse_args()
imgpath = args.image_path

# Process image
with Image.open(imgpath) as img:
    s = s_xy[0] + img.width * s_xy[1]
    t = t_xy[0] + img.width * t_xy[1]
    graph = create_graph(img, s, t)
    max_flow = calculate_max_flow(graph, s, t)
    out_img = color_image(img, graph, max_flow, s, t, s_rgb, t_rgb)
    #out_img.putpixel((0,0), (255,0,0))
    out_img.save(args.output_path)
