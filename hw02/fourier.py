from PIL import Image
import numpy as np
import argparse
import math

# Shift the zero-frequency component to the center of the spectrum
def shift_freq(ft):
    s = ft.shape
    center_w = math.ceil(s[0]/2)
    center_h = math.ceil(s[1]/2)
    for x in range(0, s[0]):
        for y in range(0, center_h):
            if x < center_w: 
                tmp = ft[x][y]
                ft[x][y] = ft[x + center_w][y + center_h]
                ft[x + center_w][y + center_h] = tmp
            else:
                tmp = ft[x][y]
                ft[x][y] = ft[x - center_w][y + center_h]
                ft[x - center_w][y + center_h] = tmp

    return ft

# Using logarithmic transformation
def monadic_FT(img, amplitude):
    out_img = Image.new('L', img.size)
    w = img.width
    h = img.height
    tmp = np.zeros((w,h))
    for x in range(0,w):
        for y in range(0,h):
            val = amplitude[y][x]       
            pixel = math.log(np.abs(val) + 1)
            tmp[x][y] = pixel
    
    mmax = np.amax(tmp) 
    mmin = np.amin(tmp) 

    for x in range(0,w):
        for y in range(0,h):   
            pixel = int(math.ceil(255 * (tmp[x][y] - mmin) / (mmax-mmin)))
            out_img.putpixel((x,y), pixel)

    return out_img

# Define arguments
parser = argparse.ArgumentParser(description='Fourier transform of the image, amplitude')
parser.add_argument('--image_path',
                    required = True,
                    help = 'Path to the image file')
parser.add_argument('--output_path',
                    required = True,
                    help = 'Path to the output file')


# Parse arguments
args = parser.parse_args()
imgpath = args.image_path

# Process image
with Image.open(imgpath) as img:
    # 2D Fourier transform
    ft = np.fft.fft2(np.array(img))

    # Shift the zero-frequency component to the center of the spectrum
    shifted = shift_freq(ft)

    # Apply monadic operation on amplitude
    out_img = monadic_FT(img, np.abs(shifted))

    # Save image
    out_img.save(args.output_path)


